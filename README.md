Silo
====

Silo is a nodejs virtualenv + npm linker tool. I created it because I couldn't 
stand npm packages littering my hdd when I wanted to do some prototyping and 
there would be slight variances in dependencies. 

## Requirements

- php 7.4+
- xclip
- nodejs
- unix-like environment

## Usage

```shell
$ silo
silo - nodejs virtualenv + npm linker tool

usage:
  silo [command] [arguments]

available commands:
  activate  generate activation code
  clone     clone existing virtual environment
  create    create a virtual environment
  linker    npm linker utility
  list      list available virtual environments
  remove    remove a virtual environment

command usage examples:
  silo activate [name]
  silo clone [existing] [new_name]
  silo create [name]
  silo linker
  silo list
  silo remove [name]

```

## Why/How would I use this tool?

This tool would be useful for someone who prototypes a lot. If you have a project 
or scaffold that you use as a base, then you can create a silo after the project 
type. Everytime you create a project of that type, you activate the silo/environment 
and run the linker. Project files are referencing 1 place on disk. It's even better 
if you automate your workflow.

## Downloads

- [silo.phar](https://bitbucket.org/prgr4m/php-silo/downloads/silo.phar)
- [silo.phar-md5.txt](https://bitbucket.org/prgr4m/php-silo/downloads/silo.phar-md5.txt)
- [silo.phar-sha1.txt](https://bitbucket.org/prgr4m/php-silo/downloads/silo.phar-sha1.txt)
- [silo.phar-sha256.txt](https://bitbucket.org/prgr4m/php-silo/downloads/silo.phar-sha256.txt)

## License

See the [license]('./LICENSE.md') file.
