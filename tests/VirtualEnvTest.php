<?php 
namespace Silo\Test;

use PHPUnit\Framework\TestCase;
use Silo\VirtualEnv;

class VirtualEnvTest extends TestCase
{
    protected function setUp(): void {
        putenv('SILO_TEST_HOME=' . sys_get_temp_dir());
        $this->make_silo_home();
    }

    protected function tearDown(): void {
        exec('rm -rf ' . $this->get_silo_home());
    }

    protected function get_silo_home(): string {
        return sys_get_temp_dir() . DIRECTORY_SEPARATOR . '.silo';
    }

    protected function make_silo_home(): void {
        $silo_home = $this->get_silo_home();
        if (!file_exists($silo_home)) mkdir($silo_home);
    }

    protected function make_test_venv(string $venv_name='testing'): string {
        VirtualEnv::create($venv_name);
        $silo_home = $this->get_silo_home();
        return $silo_home . DIRECTORY_SEPARATOR . 'testing';
    }

    public function test_get_venv_list() {
        // no venvs/dirs in app_home
        $this->expectOutputString("no available virtual environments\n");
        VirtualEnv::list();

        // test directories in output
        $silo_home = $this->get_silo_home();
        mkdir($silo_home . DIRECTORY_SEPARATOR . 'test1');
        $this->expectOutputRegex('/test1/');
        VirtualEnv::list();
    }

    public function test_create_venv() {
        $venv_loc = $this->make_test_venv();
        $this->assertDirectoryExists($venv_loc);

        $this->expectException(\Exception::class);
        VirtualEnv::create('testing'); // should throw when already exists
    }

    public function test_remove_venv() {
        $venv_loc = $this->make_test_venv();
        $this->assertDirectoryExists($venv_loc);

        VirtualEnv::remove('testing');
        $this->assertFalse(file_exists($venv_loc)); // should not exist after removal

        $this->expectException(\Exception::class);   // expect next operation to throw exception
        VirtualEnv::remove('testing');
    }

    public function test_clone_venv() {
        $venv_loc = $this->make_test_venv();
        VirtualEnv::clone('testing', 'tester');
        $new_loc = $this->get_silo_home() . DIRECTORY_SEPARATOR . 'tester';
        $this->assertDirectoryExists($new_loc);
    }
}
