<?php 
use PHPUnit\Framework\TestCase;
use Silo\SiloApp;

class SiloAppTest extends TestCase
{
    protected function setUp(): void {
        putenv('SILO_TEST_HOME=' . sys_get_temp_dir());
    }

    public function test_get_silo_home() {
        $testing_home = getenv('SILO_TEST_HOME') . DIRECTORY_SEPARATOR . '.silo';
        $this->assertEquals($testing_home, SiloApp::get_silo_home());
    }

    public function test_get_data_dir() {
        $data_dir = SiloApp::get_data_dir();
        $this->assertDirectoryExists($data_dir);
    }

    public function test_silo_app_init_process() {
        $testing_home = SiloApp::get_silo_home();
        $silo_app_1 = new SiloApp();
        $this->assertDirectoryExists($testing_home,
            "Silo didn't create its own application directory");
        unset($silo_app_1);
        system('rm -rf ' . $testing_home);
        putenv('SILO_TEST_HOME=/this/dir/does/not/exist/and/shouldnt');
        $this->expectWarning();
        $silo_app_2 = new SiloApp();
    }

    public function test_silo_app_parse_arguments() {
        $silo_app = new SiloApp();
        $test_1 = ['activate', 'name'];
        $test_2 = ['clone', 'existing', 'new_one'];
        $test_3 = ['create', 'name'];
        $test_4 = ['linker'];
        $test_5 = ['remove', 'name'];

        $silo_app->parse_arguments($test_1, true);
        $silo_app->parse_arguments($test_2, true);
        $silo_app->parse_arguments($test_3, true);
        $silo_app->parse_arguments($test_4, true);
        $silo_app->parse_arguments($test_5, true);

        $fail_1 = [];
        $fail_2 = ['meh', 'should', 'not', 'work'];
        $fail_3 = ['clone'];
        $fail_4 = ['clone', 'meh'];
        $fail_5 = ['clone', '', 'ha'];
        $fail_6 = ['list', 'all'];

        $this->expectException(\Exception::class);
        $silo_app->parse_arguments($fail_1, true);
        $silo_app->parse_arguments($fail_2, true);
        $silo_app->parse_arguments($fail_3, true);
        $silo_app->parse_arguments($fail_4, true);
        $silo_app->parse_arguments($fail_5, true);
        $silo_app->parse_arguments($fail_6, true);
    }
}
