<?php 
namespace Silo;

use Silo\SiloApp;
use Silo\NPM;

class VirtualEnv
{
    static function activate(string $venv_name): void {
        if (empty($venv_name)) exit("Cannot activate virtual environment without a name\n");
        $venvs = self::get_venv_list();
        if (empty($venvs)) exit("[!] No virtual environments to activate\n");
        if (!in_array($venv_name, $venvs)) 
            exit("[!] Virtualenv [$venv_name] doesn't exist\n");
        $app_home = SiloApp::get_silo_home();
        $path = implode(DIRECTORY_SEPARATOR, [$app_home, $venv_name, 'bin', 'activate']);
        if (!file_exists($path))
            exit("[!] Shell activation script missing from virtual environment\n");
        
        // descriptor spec -- as in shell
        $proc_desc_spec = [
            0 => ['pipe', 'r'],
            1 => ['pipe', 'w'],
            2 => ['file', '/tmp/silo-xclip-error.txt', 'a']
        ];
        $xclip_cmd = 'xclip -selection clipboard';
        $xclip_proc = proc_open($xclip_cmd, $proc_desc_spec, $pipes, NULL, NULL);
        if (!is_resource($xclip_proc)) exit("[!] xclip is not installed on the system\n\n");
        $activate_cmd = "source $path";
        fwrite($pipes[0], $activate_cmd);
        fclose($pipes[0]);
        fclose($pipes[1]);        // closing to avoid deadlock
        proc_close($xclip_proc);  // need return code?
        echo "[!] Activation code copied to the clipboard\n";
        echo "  Press: <Shift+F10><P><Enter> -- (xfce)\n";
        echo "[!] To deactivate an activated virtual environment:\n";
        echo "  Run: deactivate_node\n";
    }

    static function create(string $venv_name): void {
        if (empty($venv_name)) exit("Cannot create virtual environment without a name\n");
        $venv_root = NPM::make_npm_prefix($venv_name);
        if (file_exists($venv_root)) {
            $err_msg = "[!] Virtual environment already exists\n";
            if (!empty('SILO_TEST_HOME')) throw new \Exception($err_msg);
            echo $err_msg;
        } elseif (self::is_activated_environment()) {
            echo "[!] You are running under an activated virtual environment\n";
            echo "  To deactivate run: deactivate_node\n";
        } else {
            $config = [
                'venv_name'    => $venv_name,
                'venv_root'    => $venv_root,
                'venv_bin'     => implode(DIRECTORY_SEPARATOR, [$venv_root, 'bin']),
                'venv_modules' => NPM::make_node_modules_path($venv_name)
            ];

            // closure kept within VirtualEnv::create
            $make_venv_dir = function(array $setup_config): void {
                $src_dir = implode(DIRECTORY_SEPARATOR, [$setup_config['venv_root'], 'src']);
                if (!mkdir($setup_config['venv_modules'], 0755, true))
                    exit("Could not create virtual environment directory\n");
                mkdir($setup_config['venv_bin'], 0755);
                mkdir($src_dir, 0755);
            };

            // closure kept within VirtualEnv::create
            $setup_venv_bins = function (array $setup_config, callable $writer): void {
                $activate_fname = $setup_config['venv_bin'] . DIRECTORY_SEPARATOR . 'activate';
                $activate_contents = str_replace('SILO_ENV_PATH',
                    $setup_config['venv_root'], VirtualEnv::get_activation_script());
                $activate_contents = str_replace('SILO_ENV_NAME',
                    $setup_config['venv_name'], $activate_contents);
                $writer($activate_fname, $activate_contents);

                $shim_fname = $setup_config['venv_bin'] . DIRECTORY_SEPARATOR . 'shim';
                $shim_contents = str_replace('SILO_ENV_PATH', 
                    $setup_config['venv_root'], VirtualEnv::get_shim_script());
                $shim_contents = str_replace('SILO_ENV_MODULES_PATH',
                    $setup_config['venv_modules'], $shim_contents);
                $shim_contents = str_replace('SYSTEM_NODE', NPM::get_system_node(), 
                    $shim_contents);
                $writer($shim_fname, $shim_contents);

                $node_fname = $setup_config['venv_bin'] . DIRECTORY_SEPARATOR . 'node';
                $writer($node_fname, $shim_contents);
                $node_link = $setup_config['venv_bin'] . DIRECTORY_SEPARATOR . 'nodejs';
                symlink($node_fname, $node_link);
            };

            // closure kept within VirtualEnv::create
            $file_writer = function (string $filename, string $contents) {
                $fh = fopen($filename, 'w') or die("Could not create $filename\n");
                fwrite($fh, $contents);
                fclose($fh);
                chmod($filename, 0755);
            };

            $make_venv_dir($config);
            $setup_venv_bins($config, $file_writer);
            if (empty(getenv('SILO_TEST_HOME'))) self::activate($venv_name);
        }
    }

    static function clone(string $src_venv_name, string $new_venv_name): void {
        if (empty($src_venv_name) || empty($new_venv_name))
            exit("[!] Cannot clone virtual environment -- missing arguments\n\n");
        $src_env_path = NPM::make_npm_prefix($src_venv_name);
        if (!file_exists($src_env_path))
            exit("[!] Virtual environment [$srv_venv_name} doesn't exist\n\n");
        $new_env_path = NPM::make_npm_prefix($new_venv_name);
        $cmd = escapeshellcmd("cp -r $src_env_path $new_env_path");
        system($cmd);

        function setup_clone_bin_dir(string $venv_path, string $old_name, string $new_name) {
            $bin_dir = $venv_path . DIRECTORY_SEPARATOR . 'bin';
            $target_files = ['node', 'activate', 'shim'];
            foreach ($target_files as $target) {
                $file = $bin_dir . DIRECTORY_SEPARATOR . $target;
                $orig_contents = file_get_contents($file);
                $contents = str_replace($old_name, $new_name, $orig_contents);
                $fh = fopen($file, 'w');
                fwrite($fh, $contents);
                fclose($fh);
            }
        }

        setup_clone_bin_dir($new_env_path, $src_venv_name, $new_venv_name);
        if (empty(getenv('SILO_TEST_HOME'))) self::activate($new_venv_name);
    }

    static function remove(string $venv_name): void {
        if (empty($venv_name)) 
            exit("[!] Cannot remove a virtual environment if name not provided\n");
        if (self::is_same_environment($venv_name))
            exit("[!] Deactivate virtual environment with 'deactivate_node' first\n");
        $venv_path = NPM::make_npm_prefix($venv_name);
        if (!is_dir($venv_path))
            exit("[!] Virtual environment [$venv_name] doesn't exist\n");
        $cmd = escapeshellcmd("rm -rf $venv_path");
        exec($cmd);
        echo "Removed virtual environment [$venv_name]\n";
    }

    static function list(): void {
        $venvs = self::get_venv_list();
        if (count($venvs) > 0) {
            echo "available virtual environments:\n";
            foreach ($venvs as $venv) echo "  $venv\n";
        } else echo "no available virtual environments\n";
    }

    static function get_venv_list(): array {
        return array_slice(scandir(SiloApp::get_silo_home()), 2);
    }

    static function is_activated_environment(): bool {
        return strpos(NPM::get_npm_prefix(), SiloApp::get_silo_home()) !== false;
    }

    static function is_same_environment(string $venv_name): bool {
        if (!self::is_activated_environment()) return false;
        $requested_root = NPM::make_npm_prefix($venv_name);
        $current_root = NPM::get_npm_prefix();
        if ($requested_root == $current_root) return true;
        return false;
    }

    static function get_activation_script(): string {
        $activation_script = SiloApp::get_data_dir() . DIRECTORY_SEPARATOR . 'activate';
        if (!file_exists($activation_script)) exit("Cannot find activation script template\n");
        return file_get_contents($activation_script);
    }

    static function get_shim_script(): string {
        $shim_script = SiloApp::get_data_dir() . DIRECTORY_SEPARATOR . 'shim';
        if (!file_exists($shim_script)) exit("Cannot find shim script template\n");
        return file_get_contents($shim_script);
    }
}
