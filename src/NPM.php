<?php
namespace Silo;

use Silo\SiloApp;
use Silo\VirtualEnv;

class NPM 
{
    static function get_npm_prefix(): string {
        $prefix_cmd = "npm config get prefix";
        return trim(shell_exec($prefix_cmd));
    }

    static function get_node_modules_path(): string {
        $prefix = self::get_npm_prefix();
        return implode(DIRECTORY_SEPARATOR, [$prefix, 'lib', 'node_modules']);
    }

    static function get_system_node(): string {
        return trim(shell_exec("which node"));
    }

    static function make_npm_prefix(string $venv_name): string {
        return SiloApp::get_silo_home() . DIRECTORY_SEPARATOR . $venv_name;
    }

    static function make_node_modules_path(string $venv_name): string {
        $new_prefix = self::make_npm_prefix($venv_name);
        return implode(DIRECTORY_SEPARATOR, [$new_prefix, 'lib', 'node_modules']);
    }

    static function linker() {
        if (!VirtualEnv::is_activated_environment())
            exit("[!] Run linker under an activated virtual environment\n");
        $package_parser = function (string $pkg='package.json'): array {
            if (!file_exists($pkg)) exit("$pkg not in current directory\n");
            $pkg_contents = json_decode(file_get_contents($pkg), true);
            $deps = [];
            if (array_key_exists('dependencies', $pkg_contents))
                $deps = array_merge($deps, $pkg_contents['dependencies']);
            if (array_key_exists('devDependencies', $pkg_contents))
                $deps = array_merge($deps, $pkg_contents['devDependencies']);
            return $deps;
        };
        $project_deps = $package_parser();
        $global_path = self::get_node_modules_path();
        $local_pkgs = array_keys($project_deps);

        // removing '.' and '..' in the listing
        $global_listing = array_slice(scandir($global_path), 2);
        $missing_pkgs = array_diff($local_pkgs, $global_listing);

        if (count($missing_pkgs) > 0) {
            echo "[!] Installing missing packages\n";
            $global_install_cmd = "npm install -g --silent";
            foreach ($missing_pkgs as $pkg) {
                $pkg_install = $pkg . "@" . $project_deps[$pkg];
                system("$global_install_cmd $pkg_install");
            }
        }
        echo "[!]: Creating links\n";
        foreach ($local_pkgs as $pkg)
            system("npm link $pkg --silent");
    }
}
