<?php declare(strict_types=1);
namespace Silo;

class SiloApp 
{
    private string $_home_dir;

    public function __construct() {
        $this->_init_check();
    }

    private function _init_check(): void {
        $home_dir = self::get_silo_home();
        if (!file_exists($home_dir))
            if (!mkdir($home_dir, 0700)) // E_WARNING created if failed
                exit("Failed to create application home directory\n");
        $this->_home_dir = $home_dir;
    }

    public function parse_arguments(array $args_in = [], bool $testing=false): void {
        if (empty($args_in)) throw new \Exception("Silo needs arguments to parse");
        $control_map = [ // key => [ args_expected, callable, err_msg ]
            'activate' => [ 1, 'Silo\VirtualEnv::activate', '[name]' ],
            'clone'    => [ 2, 'Silo\VirtualEnv::clone', '[existing] [new_name]' ],
            'create'   => [ 1, 'Silo\VirtualEnv::create', '[name]' ],
            'linker'   => [ 0, 'Silo\NPM::linker', '' ],
            'list'     => [ 0, 'Silo\VirtualEnv::list', '' ],
            'remove'   => [ 1, 'Silo\VirtualEnv::remove', '[name]' ]
        ]; // so much cleaner than using a switch
        if (!array_key_exists($args_in[0], $control_map)) {
            $error_message = "There is no '{$args_in[0]}' command\n";
            if ($testing)
                throw new \Exception($error_message);
            else
                exit($error_message);
        }
        $key = array_shift($args_in);
        $user_args = array_filter($args_in, fn($arg) => !empty($arg));
        if (count($user_args) != $control_map[$key][0]) {
            global $argv;
            $error_message = <<<ERR
[!] Invalid number of arguments passed in for command

command usage:
  {$argv[0]} $key {$control_map[$key][2]}


ERR;
            if ($testing)
                throw new \Exception($error_message);
            else
                exit($error_message);
        }
        if ($testing) return;
        if ($control_map[$key][0] > 0) 
            call_user_func_array($control_map[$key][1], $user_args);
        else
            call_user_func($control_map[$key][1]);
    }

    static function get_silo_home(): string {
        $app_root = (!empty(getenv('SILO_TEST_HOME'))) 
            ? getenv('SILO_TEST_HOME') : getenv('HOME'); 
        return $app_root . DIRECTORY_SEPARATOR . '.silo'; 
    } 

    static function get_data_dir(): string {
        return dirname(__FILE__) . '/../data';
    }
}
